# RBAC模型模拟

#### 项目描述

1. 采用Spring boot与vue3搭建的网站，模拟权限管理的效果
2. 功能列表
```
1. 角色管理
2. 菜单管理
3. 用户管理
4. 图片管理
5. 图标管理
6. 用户端功能
```

#### 演示截图

1. 登陆页面
![登陆页面](asset/img1.png)
2. 注册页面
![注册页面](asset/img2.png)
3. 角色管理页面
![角色管理页面](asset/img3.png)
![修改角色](asset/img4.png)
![修改角色菜单](asset/img5.png)
4. 菜单管理页面
![菜单管理页面](asset/img6.png)
![查看文件内容](asset/img7.png)
![修改菜单](asset/img8.png)
![编辑文件内容](asset/img9.png)
5. 用户管理页面
![用户管理页面](asset/img10.png)
![修改用户](asset/img11.png)
6. 图片管理页面
![图片管理页面](asset/img12.png)
![修改图片](asset/img13.png)
7. 图标管理页面
![图标管理页面](asset/img14.png)
![修改图标](asset/img15.png)
8. 用户端功能展示
![系统首页](asset/img16.png)
![测试菜单](asset/img17.png)

