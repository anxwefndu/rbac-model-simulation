import {createRouter, createWebHashHistory} from 'vue-router'

import Home from '@/views/Home'
import Login from '@/views/Login'
import Register from '@/views/Register'
import Error from '@/views/Error'
import Index from '@/views/Index'
import RoleManage from '@/views/RoleManage'
import MenuManage from '@/views/MenuManage'
import UserManage from '@/views/UserManage'
import ImageManage from '@/views/ImageManage'
import IcoManage from '@/views/IcoManage'

const homeRoute = {
    path: '/home',
    name: "Home",
    component: Home,
    redirect: '/index',
    children: [
        {
            path: '/index',
            component: Index,
        },
        {
            path: '/roleManage',
            component: RoleManage,
        },
        {
            path: '/menuManage',
            component: MenuManage,
        },
        {
            path: '/userManage',
            component: UserManage,
        },
        {
            path: '/imageManage',
            component: ImageManage,
        },
        {
            path: '/icoManage',
            component: IcoManage,
        },
    ]
};

export function initRoute(menuList) {
    homeRoute.children.splice(6, homeRoute.children.length - 6);
    for (let i = 0; i < menuList.length; i++) {
        if (menuList[i].menuId > 0) {
            homeRoute.children.push({
                path: menuList[i].menuPath,
                component: () => import("@/views/" + menuList[i].fileName)
            });
        }
    }
    router.removeRoute("Home");
    router.addRoute(homeRoute);
}

const routes = [
    {
        path: '/',
        redirect: '/login'
    },
    {
        path: '/login',
        component: Login,
    },
    {
        path: '/register',
        component: Register,
    },
    {
        path: '/:pathMatch(.*)',
        component: Error,
    },
]

const router = createRouter({
    history: createWebHashHistory(),
    routes
})

router.addRoute(homeRoute);

export default router