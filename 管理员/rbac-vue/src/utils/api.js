import axios from "axios";
import {ElMessage} from 'element-plus';

const baseUrl = 'http://127.0.0.1:9091';

export function message(msg, type) {
    ElMessage({
        message: msg,
        showClose: true,
        type: type,
        center: true
    })
}

export const axiosRequest = async (method, path, params, data, success, error = (res) => {
    message(res.data.msg, "error");
}) => {
    const res = await axios({
        method: method,
        url: baseUrl + path,
        data: data,
        params: params,
        headers: {
            username: localStorage.getItem("username"),
            token: localStorage.getItem("token")
        }
    });
    if (res.data.code === 200) {
        success(res);
    } else if (res.data.code === 500) {
        error(res);
    }
}

export async function showImageFilePicker() {
    let fileHandle;
    try {
        const pickerOpts = {
            types: [
                {
                    description: "Images",
                    accept: {
                        "image/*": [".png", ".gif", ".jpeg", ".jpg"],
                    },
                },
            ],
            excludeAcceptAllOption: true,
            multiple: false,
        };
        fileHandle = await window.showOpenFilePicker(pickerOpts);
    } catch (e) {
        if (e.name === 'AbortError' && e.message === 'The user aborted a request.') {
            return;
        } else {
            throw e;
        }
    }

    const file = await fileHandle[0].getFile();
    if (file.size > 1024 * 1024 * 10) {
        message("上传文件过大,最多不超过10MB", "warning");
        return;
    }
    return file;
}

export async function showIcoFilePicker() {
    let fileHandle;
    try {
        const pickerOpts = {
            types: [
                {
                    description: "Svg",
                    accept: {
                        "svg/*": [".svg"],
                    },
                },
            ],
            excludeAcceptAllOption: true,
            multiple: false,
        };
        fileHandle = await window.showOpenFilePicker(pickerOpts);
    } catch (e) {
        if (e.name === 'AbortError' && e.message === 'The user aborted a request.') {
            return;
        } else {
            throw e;
        }
    }

    const file = await fileHandle[0].getFile();
    if (file.size > 1024 * 1024 * 10) {
        message("上传文件过大,最多不超过10MB", "warning");
        return;
    }
    return file;
}

export function getImage(imagePath) {
    return baseUrl + "/imageManage/getImage/" + imagePath;
}

export function getIco(icoPath) {
    return baseUrl + "/icoManage/getIco/" + icoPath;
}

export const downloadFileToLocal = (href, filename) => {
    const eleLink = document.createElement('a');
    eleLink.download = filename;
    eleLink.style.display = 'none';
    eleLink.href = baseUrl + href;
    document.body.appendChild(eleLink);
    eleLink.click();
    document.body.removeChild(eleLink);
}
