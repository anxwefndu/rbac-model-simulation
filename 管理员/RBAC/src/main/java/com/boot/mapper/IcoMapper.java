package com.boot.mapper;

import com.boot.entity.Ico;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author bbyh
 * @since 2024-05-13
 */
public interface IcoMapper extends BaseMapper<Ico> {

}
