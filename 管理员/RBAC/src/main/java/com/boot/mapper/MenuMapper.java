package com.boot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.boot.entity.Menu;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author bbyh
 * @since 2023-07-11
 */
public interface MenuMapper extends BaseMapper<Menu> {

}
