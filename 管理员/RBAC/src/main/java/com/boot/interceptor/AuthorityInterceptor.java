package com.boot.interceptor;

import com.boot.entity.Result;
import com.boot.util.JwtUtil;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author admin
 * @description 权限管理，采用请求路径前缀的形式来进行权限控制，不能进行很精细的控制，但也差强人意了
 * @since 2023-03-31
 */
@Component
@Slf4j
public class AuthorityInterceptor implements HandlerInterceptor {
    private static final String OPTIONS = "OPTIONS";

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public boolean preHandle(HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull Object handler) {
        if (OPTIONS.equals(request.getMethod())) {
            return true;
        }

        try {
            String username = request.getHeader("username");
            String token = request.getHeader("token");
            if (username == null || token == null) {
                Result.writeResult(response, "token不合法");
                return false;
            }

            JwtUtil.verify(token);

            String s = stringRedisTemplate.opsForValue().get(username);
            if (!token.equals(s)) {
                Result.writeResult(response, "token不合法");
                return false;
            }
            return true;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            Result.writeResult(response, "身份验证不通过");
            return false;
        }
    }
}
