package com.boot.config;

import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.boot.entity.Menu;
import com.boot.interceptor.AuthorityInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author admin
 * @date 2023/3/31 0031 18:21
 */
@Configuration
@Slf4j
public class WebConfig implements WebMvcConfigurer {
    public static final String DEFAULT_USERNAME;
    public static final String DEFAULT_PASSWORD;
    public static final List<Menu> DEFAULT_MENU_LIST = new ArrayList<>();
    private static final List<String> AUTHORITY_EXCLUDE_PATH = new ArrayList<>();
    public static String IMAGE_PATH;
    public static String ICO_PATH;
    public static String VUE_PROJECT_PATH;

    static {
        AUTHORITY_EXCLUDE_PATH.add("/user/login");
        AUTHORITY_EXCLUDE_PATH.add("/imageManage/getImage/*");
        AUTHORITY_EXCLUDE_PATH.add("/icoManage/getIco/*");
        AUTHORITY_EXCLUDE_PATH.add("/menuManage/downloadUserPageCode");
    }

    static {
        DEFAULT_USERNAME = UUID.randomUUID().toString();
        DEFAULT_PASSWORD = UUID.randomUUID().toString();
        log.info("DEFAULT_USERNAME:" + DEFAULT_USERNAME);
        log.info("DEFAULT_PASSWORD:" + DEFAULT_PASSWORD);
    }

    static {
        DEFAULT_MENU_LIST.add(new Menu(0, "角色管理", "/roleManage", ""));
        DEFAULT_MENU_LIST.add(new Menu(0, "菜单管理", "/menuManage", ""));
        DEFAULT_MENU_LIST.add(new Menu(0, "用户管理", "/userManage", ""));
        DEFAULT_MENU_LIST.add(new Menu(0, "图片管理", "/imageManage", ""));
        DEFAULT_MENU_LIST.add(new Menu(0, "图标管理", "/icoManage", ""));
    }

    @Resource
    private AuthorityInterceptor authorityInterceptor;

    @Value("${image-path}")
    public void setImagePath(String imagePath) {
        IMAGE_PATH = imagePath;
    }

    @Value("${ico-path}")
    public void setIcoPath(String icoPath) {
        ICO_PATH = icoPath;
    }

    @Value("${vue-project-path}")
    public void setVueProjectPath(String vueProjectPath) {
        VUE_PROJECT_PATH = vueProjectPath;
    }

    @Override
    public void addInterceptors(@NotNull InterceptorRegistry registry) {
        registry.addInterceptor(authorityInterceptor).addPathPatterns("/**").excludePathPatterns(AUTHORITY_EXCLUDE_PATH);
    }

    @Override
    public void addCorsMappings(@NotNull CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowedMethods("GET", "POST", "DELETE", "PUT")
                .maxAge(3600);
    }

    @Bean
    public MybatisPlusInterceptor paginationInterceptor() {
        MybatisPlusInterceptor mybatisPlusInterceptor = new MybatisPlusInterceptor();
        PaginationInnerInterceptor paginationInnerInterceptor = new PaginationInnerInterceptor();
        paginationInnerInterceptor.setOverflow(true);
        mybatisPlusInterceptor.addInnerInterceptor(paginationInnerInterceptor);
        return mybatisPlusInterceptor;
    }
}
