package com.boot.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author bbyh
 * @since 2024-05-13
 */
@Getter
@Setter
public class Ico implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 图标ID
     */
    @TableId(value = "ico_id", type = IdType.AUTO)
    private Integer icoId;

    /**
     * 图标名称
     */
    private String icoName;

    /**
     * 图标保存路径
     */
    private String icoPath;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
}
