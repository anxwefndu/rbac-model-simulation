package com.boot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.boot.entity.Image;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author bbyh
 * @since 2024-05-11
 */
public interface IImageService extends IService<Image> {

}
