package com.boot.service;

import com.boot.entity.Ico;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bbyh
 * @since 2024-05-13
 */
public interface IIcoService extends IService<Ico> {

}
