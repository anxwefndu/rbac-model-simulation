package com.boot.service.impl;

import com.boot.entity.Ico;
import com.boot.mapper.IcoMapper;
import com.boot.service.IIcoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author bbyh
 * @since 2024-05-13
 */
@Service
public class IcoServiceImpl extends ServiceImpl<IcoMapper, Ico> implements IIcoService {

}
