package com.boot.controller;

import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.boot.entity.Image;
import com.boot.entity.Result;
import com.boot.service.IImageService;
import com.boot.util.FileUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.UUID;

import static com.boot.config.WebConfig.IMAGE_PATH;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author bbyh
 * @since 2024-05-11
 */
@RestController
@RequestMapping("/imageManage")
@Api("图片接口")
@Slf4j
public class ImageManageController {
    @Resource
    private IImageService imageService;

    @ApiOperation("图片获取接口")
    @GetMapping("/getImage/{imagePath}")
    public void getImageList(@PathVariable String imagePath, HttpServletResponse response) {
        try {
            ServletOutputStream outputStream = response.getOutputStream();
            FileUtil.download(outputStream, imagePath, IMAGE_PATH);
            outputStream.flush();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            Result.writeResult(response, "图片获取接口服务出错");
        }
    }

    @ApiOperation("图片列表获取接口")
    @PostMapping("/getImageList")
    public Result getImageList(@RequestBody Map<String, Object> map) {
        try {
            Integer current = (Integer) map.get("current");
            Integer pageSize = (Integer) map.get("pageSize");
            String imageName = (String) map.get("imageName");

            LambdaQueryWrapper<Image> wrapper = new LambdaQueryWrapper<>();
            wrapper.like(Image::getImageName, imageName);

            Page<Image> page = new Page<>(current, pageSize);
            Page<Image> imagePage = imageService.page(page, wrapper);
            JSONObject jsonObject = new JSONObject();
            jsonObject.putOnce("imageList", imagePage.getRecords());
            jsonObject.putOnce("total", imagePage.getTotal());
            return Result.success("图片列表获取成功", jsonObject);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("图片列表获取接口服务出错", null);
        }
    }

    @ApiOperation("图片添加接口")
    @PostMapping("/addImage")
    @Transactional(rollbackFor = Exception.class)
    public Result addImage(@RequestBody MultipartFile file) {
        try {
            String originalFilename = file.getOriginalFilename();
            String uuid = UUID.randomUUID().toString();
            assert originalFilename != null;
            String imagePath = uuid + originalFilename.substring(originalFilename.lastIndexOf("."));
            FileUtil.upload(file.getInputStream(), imagePath, IMAGE_PATH);

            Image image = new Image();
            image.setImageName(originalFilename);
            image.setImagePath(imagePath);
            image.setCreateTime(LocalDateTime.now());
            imageService.save(image);

            return Result.success("图片添加成功", null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("图片添加接口服务出错", null);
        }
    }

    @ApiOperation("图片修改接口")
    @PostMapping("/editImage")
    @Transactional(rollbackFor = Exception.class)
    public Result editImage(@RequestBody MultipartFile file, @RequestParam Integer imageId, @RequestParam String imageName) {
        try {
            LambdaQueryWrapper<Image> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(Image::getImageId, imageId);
            Image image = imageService.getOne(wrapper);
            if (image == null) {
                return Result.error("图片不存在，修改失败", null);
            }
            FileUtil.delete(image.getImagePath(), IMAGE_PATH);

            String uuid = UUID.randomUUID().toString();
            String imagePath = uuid + imageName.substring(imageName.lastIndexOf("."));
            FileUtil.upload(file.getInputStream(), imagePath, IMAGE_PATH);

            image.setImageName(imageName);
            image.setImagePath(imagePath);
            imageService.update(image, wrapper);

            return Result.success("图片修改成功", null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("图片修改接口服务出错", null);
        }
    }

    @ApiOperation("图片删除接口")
    @PostMapping("/deleteImage")
    @Transactional(rollbackFor = Exception.class)
    public Result deleteImage(@RequestBody Map<String, Object> map) {
        try {
            Integer imageId = (Integer) map.get("imageId");
            LambdaQueryWrapper<Image> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(Image::getImageId, imageId);
            Image image = imageService.getOne(wrapper);
            if (image == null) {
                return Result.error("图片不存在，删除失败", null);
            }
            FileUtil.delete(image.getImagePath(), IMAGE_PATH);
            imageService.remove(wrapper);
            return Result.success("图片删除成功", null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("图片删除接口服务出错", null);
        }
    }

}
