package com.boot.controller;

import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.boot.entity.Ico;
import com.boot.entity.Result;
import com.boot.service.IIcoService;
import com.boot.util.FileUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.UUID;

import static com.boot.config.WebConfig.ICO_PATH;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author bbyh
 * @since 2024-05-13
 */
@RestController
@RequestMapping("/icoManage")
@Api("图标接口")
@Slf4j
public class IcoController {
    @Resource
    private IIcoService icoService;

    @ApiOperation("图标获取接口")
    @GetMapping("/getIco/{icoPath}")
    public void getIcoList(@PathVariable String icoPath, HttpServletResponse response) {
        try {
            response.setContentType("image/svg+xml");
            ServletOutputStream outputStream = response.getOutputStream();
            FileUtil.download(outputStream, icoPath, ICO_PATH);
            outputStream.flush();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            Result.writeResult(response, "图标获取接口服务出错");
        }
    }

    @ApiOperation("图标列表获取接口")
    @PostMapping("/getIcoList")
    public Result getIcoList(@RequestBody Map<String, Object> map) {
        try {
            Integer current = (Integer) map.get("current");
            Integer pageSize = (Integer) map.get("pageSize");
            String icoName = (String) map.get("icoName");

            LambdaQueryWrapper<Ico> wrapper = new LambdaQueryWrapper<>();
            wrapper.like(Ico::getIcoName, icoName);

            Page<Ico> page = new Page<>(current, pageSize);
            Page<Ico> icoPage = icoService.page(page, wrapper);
            JSONObject jsonObject = new JSONObject();
            jsonObject.putOnce("icoList", icoPage.getRecords());
            jsonObject.putOnce("total", icoPage.getTotal());
            return Result.success("图标列表获取成功", jsonObject);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("图标列表获取接口服务出错", null);
        }
    }

    @ApiOperation("图标添加接口")
    @PostMapping("/addIco")
    @Transactional(rollbackFor = Exception.class)
    public Result addIco(@RequestBody MultipartFile file) {
        try {
            String originalFilename = file.getOriginalFilename();
            String uuid = UUID.randomUUID().toString();
            assert originalFilename != null;
            String icoPath = uuid + originalFilename.substring(originalFilename.lastIndexOf("."));
            FileUtil.upload(file.getInputStream(), icoPath, ICO_PATH);

            Ico ico = new Ico();
            ico.setIcoName(originalFilename);
            ico.setIcoPath(icoPath);
            ico.setCreateTime(LocalDateTime.now());
            icoService.save(ico);

            return Result.success("图标添加成功", null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("图标添加接口服务出错", null);
        }
    }

    @ApiOperation("图标修改接口")
    @PostMapping("/editIco")
    @Transactional(rollbackFor = Exception.class)
    public Result editIco(@RequestBody MultipartFile file, @RequestParam Integer icoId, @RequestParam String icoName) {
        try {
            LambdaQueryWrapper<Ico> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(Ico::getIcoId, icoId);
            Ico ico = icoService.getOne(wrapper);
            if (ico == null) {
                return Result.error("图标不存在，修改失败", null);
            }
            FileUtil.delete(ico.getIcoPath(), ICO_PATH);

            String uuid = UUID.randomUUID().toString();
            String icoPath = uuid + icoName.substring(icoName.lastIndexOf("."));
            FileUtil.upload(file.getInputStream(), icoPath, ICO_PATH);

            ico.setIcoName(icoName);
            ico.setIcoPath(icoPath);
            icoService.update(ico, wrapper);

            return Result.success("图标修改成功", null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("图标修改接口服务出错", null);
        }
    }

    @ApiOperation("图标删除接口")
    @PostMapping("/deleteIco")
    @Transactional(rollbackFor = Exception.class)
    public Result deleteIco(@RequestBody Map<String, Object> map) {
        try {
            Integer icoId = (Integer) map.get("icoId");
            LambdaQueryWrapper<Ico> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(Ico::getIcoId, icoId);
            Ico ico = icoService.getOne(wrapper);
            if (ico == null) {
                return Result.error("图标不存在，删除失败", null);
            }
            FileUtil.delete(ico.getIcoPath(), ICO_PATH);
            icoService.remove(wrapper);
            return Result.success("图标删除成功", null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("图标删除接口服务出错", null);
        }
    }
}
