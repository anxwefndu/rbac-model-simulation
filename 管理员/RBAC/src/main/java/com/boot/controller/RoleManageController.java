package com.boot.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.boot.entity.Menu;
import com.boot.entity.Result;
import com.boot.entity.Role;
import com.boot.entity.RoleMenu;
import com.boot.service.IMenuService;
import com.boot.service.IRoleMenuService;
import com.boot.service.IRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.*;

import static com.boot.config.WebConfig.DEFAULT_MENU_LIST;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author bbyh
 * @since 2023-07-11
 */
@RestController
@RequestMapping("/roleManage")
@Api("角色接口")
@Slf4j
public class RoleManageController {
    @Resource
    private IRoleService roleService;
    @Resource
    private IMenuService menuService;
    @Resource
    private IRoleMenuService roleMenuService;

    @ApiOperation("用户菜单获取接口")
    @GetMapping("/getUserRoleMenu")
    public Result getUserRoleMenu() {
        try {
            List<Menu> menuList = new ArrayList<>(DEFAULT_MENU_LIST);
            menuList.addAll(menuService.list());
            return Result.success("用户菜单获取成功", menuList);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("用户菜单获取接口服务出错", null);
        }
    }

    @ApiOperation("角色菜单获取接口")
    @PostMapping("/getRoleMenu")
    public Result getRoleMenu(@RequestBody Map<String, Object> map) {
        try {
            Integer roleId = (Integer) map.get("roleId");
            String roleName = (String) map.get("roleName");

            Map<String, Object> responseRoleMenu = new HashMap<>();
            responseRoleMenu.put("roleId", roleId);
            responseRoleMenu.put("label", roleName);
            responseRoleMenu.put("isLeaf", false);

            LambdaQueryWrapper<RoleMenu> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(RoleMenu::getRoleId, roleId);
            List<RoleMenu> roleMenuList = roleMenuService.list(wrapper);
            HashSet<Integer> roleMenuIdSet = new HashSet<>();
            for (RoleMenu roleMenu : roleMenuList) {
                roleMenuIdSet.add(roleMenu.getMenuId());
            }

            List<Menu> menuList = menuService.list();
            ArrayList<Map<String, Object>> children = new ArrayList<>();
            for (Menu menu : menuList) {
                Map<String, Object> child = new HashMap<>();
                child.put("menuId", menu.getMenuId());
                child.put("label", menu.getMenuName());
                child.put("menuPath", menu.getMenuPath());
                child.put("checked", roleMenuIdSet.contains(menu.getMenuId()));
                child.put("isLeaf", true);
                child.put("roleId", roleId);
                children.add(child);
            }
            responseRoleMenu.put("children", children);

            return Result.success("角色菜单获取成功", responseRoleMenu);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("角色菜单获取接口服务出错", null);
        }
    }

    @ApiOperation("角色菜单修改接口")
    @PostMapping("/editRoleMenu")
    @Transactional(rollbackFor = Exception.class)
    public Result editRoleMenu(@RequestBody Map<String, Object> map) {
        try {
            Integer roleId = (Integer) map.get("roleId");

            LambdaQueryWrapper<RoleMenu> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(RoleMenu::getRoleId, roleId);
            roleMenuService.remove(wrapper);
            List<RoleMenu> roleMenuList = JSONObject.parseArray((String) map.get("roleMenuList"), RoleMenu.class);
            roleMenuService.saveBatch(roleMenuList);

            return Result.success("角色菜单修改成功", null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("角色菜单修改接口服务出错", null);
        }
    }

    @ApiOperation("角色列表获取接口")
    @PostMapping("/getRoleList")
    public Result getRoleList(@RequestBody Map<String, Object> map) {
        try {
            Integer current = (Integer) map.get("current");
            Integer pageSize = (Integer) map.get("pageSize");
            String roleName = (String) map.get("roleName");

            LambdaQueryWrapper<Role> wrapper = new LambdaQueryWrapper<>();
            wrapper.like(Role::getRoleName, roleName);

            Page<Role> page = new Page<>(current, pageSize);
            Page<Role> rolePage = roleService.page(page, wrapper);
            cn.hutool.json.JSONObject jsonObject = new cn.hutool.json.JSONObject();
            jsonObject.putOnce("roleList", rolePage.getRecords());
            jsonObject.putOnce("total", rolePage.getTotal());
            return Result.success("角色列表获取成功", jsonObject);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("角色列表获取接口服务出错", null);
        }
    }

    @ApiOperation("角色添加接口")
    @PostMapping("/addRole")
    @Transactional(rollbackFor = Exception.class)
    public Result addRole(@RequestBody Map<String, String> map) {
        try {
            String roleName = map.get("roleName");
            LambdaQueryWrapper<Role> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(Role::getRoleName, roleName);
            Role role = roleService.getOne(wrapper);
            if (role != null) {
                return Result.error("该角色已存在，请重新添加", null);
            }

            role = new Role();
            role.setRoleName(roleName);
            roleService.save(role);

            return Result.success("角色添加成功", null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("角色添加接口服务出错", null);
        }
    }

    @ApiOperation("角色修改接口")
    @PostMapping("/editRole")
    @Transactional(rollbackFor = Exception.class)
    public Result editRole(@RequestBody Map<String, Object> map) {
        try {
            Integer roleId = (Integer) map.get("roleId");
            LambdaQueryWrapper<Role> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(Role::getRoleId, roleId);
            Role role = roleService.getOne(wrapper);
            if (role == null) {
                return Result.error("该角色不存在，修改失败", null);
            }

            String roleName = (String) map.get("roleName");
            LambdaQueryWrapper<Role> notExistWrapper = new LambdaQueryWrapper<>();
            notExistWrapper.ne(Role::getRoleId, roleId).and(roleWrapper -> roleWrapper.eq(Role::getRoleName, roleName));
            Role notExist = roleService.getOne(notExistWrapper);
            if (notExist != null) {
                return Result.error("该角色已存在，请重新修改", null);
            }

            role.setRoleName(roleName);
            roleService.update(role, wrapper);
            return Result.success("角色修改成功", null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("角色修改接口服务出错", null);
        }
    }

    @ApiOperation("角色删除接口")
    @PostMapping("/deleteRole")
    @Transactional(rollbackFor = Exception.class)
    public Result deleteRole(@RequestBody Map<String, Object> map) {
        try {
            Integer roleId = (Integer) map.get("roleId");
            LambdaQueryWrapper<Role> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(Role::getRoleId, roleId);
            Role role = roleService.getOne(wrapper);
            if (role == null) {
                return Result.error("该角色不存在，删除失败", null);
            }
            roleService.remove(wrapper);
            return Result.success("角色删除成功", null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("角色删除接口服务出错", null);
        }
    }
}
