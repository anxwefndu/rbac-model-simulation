package com.boot.controller;

import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.boot.entity.Result;
import com.boot.entity.User;
import com.boot.service.IRoleService;
import com.boot.service.IUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author bbyh
 * @since 2023-07-11
 */
@RestController
@RequestMapping("/userManage")
@Api("用户接口")
@Slf4j
public class UserManageController {
    @Resource
    private IUserService userService;
    @Resource
    private IRoleService roleService;

    @ApiOperation("角色列表获取接口")
    @GetMapping("/getRoleList")
    public Result getRoleList() {
        try {
            return Result.success("角色列表获取成功", roleService.list());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("角色列表获取接口服务出错", null);
        }
    }

    @ApiOperation("用户列表获取接口")
    @PostMapping("/getUserList")
    public Result getUserList(@RequestBody Map<String, Object> map) {
        try {
            Integer current = (Integer) map.get("current");
            Integer pageSize = (Integer) map.get("pageSize");
            String username = (String) map.get("username");

            LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
            wrapper.like(User::getUsername, username);

            Page<User> page = new Page<>(current, pageSize);
            Page<User> userPage = userService.page(page, wrapper);
            JSONObject jsonObject = new JSONObject();
            jsonObject.putOnce("userList", userPage.getRecords());
            jsonObject.putOnce("total", userPage.getTotal());
            return Result.success("用户列表获取成功", jsonObject);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("用户列表获取接口服务出错", null);
        }
    }

    @ApiOperation("用户修改接口")
    @PostMapping("/editUser")
    @Transactional(rollbackFor = Exception.class)
    public Result editUser(@RequestBody Map<String, Object> map) {
        try {
            Integer id = (Integer) map.get("id");
            LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(User::getId, id);
            User user = userService.getOne(wrapper);
            if (user == null) {
                return Result.error("该用户不存在，修改失败", null);
            }

            String username = (String) map.get("username");
            LambdaQueryWrapper<User> notExistWrapper = new LambdaQueryWrapper<>();
            notExistWrapper.ne(User::getId, id).and(roleWrapper -> roleWrapper.eq(User::getUsername, username));
            User notExist = userService.getOne(notExistWrapper);
            if (notExist != null) {
                return Result.error("该用户已存在，请重新修改", null);
            }

            String password = (String) map.get("password");
            Integer roleId = (Integer) map.get("roleId");

            user.setUsername(username);
            user.setPassword(password);
            user.setRoleId(roleId);
            userService.update(user, wrapper);
            return Result.success("用户修改成功", null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("用户修改接口服务出错", null);
        }
    }

    @ApiOperation("用户删除接口")
    @PostMapping("/deleteUser")
    @Transactional(rollbackFor = Exception.class)
    public Result deleteUser(@RequestBody Map<String, Object> map) {
        try {
            Integer id = (Integer) map.get("id");
            LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(User::getId, id);
            User user = userService.getOne(wrapper);
            if (user == null) {
                return Result.error("该用户不存在，删除失败", null);
            }
            userService.remove(wrapper);
            return Result.success("用户删除成功", null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("用户删除接口服务出错", null);
        }
    }
}
