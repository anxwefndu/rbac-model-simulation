package com.boot.controller;

import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.boot.entity.Ico;
import com.boot.entity.Image;
import com.boot.entity.Menu;
import com.boot.entity.Result;
import com.boot.service.IIcoService;
import com.boot.service.IImageService;
import com.boot.service.IMenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static com.boot.config.WebConfig.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author bbyh
 * @since 2023-07-11
 */
@RestController
@RequestMapping("/menuManage")
@Api("菜单接口")
@Slf4j
public class MenuManageController {
    private static final String DEFAULT_TEXT = "<script setup>\n\n" +
            "</script>\n\n" +
            "<template>\n" +
            "\t<h2>默认效果</h2>\n" +
            "</template>\n\n" +
            "<style lang=\"scss\" scoped>\n\n" +
            "</style>\n";
    @Resource
    private IMenuService menuService;
    @Resource
    private IImageService imageService;
    @Resource
    private IIcoService icoService;

    @ApiOperation("菜单列表获取接口")
    @PostMapping("/getMenuList")
    public Result getMenuList(@RequestBody Map<String, Object> map) {
        try {
            Integer current = (Integer) map.get("current");
            Integer pageSize = (Integer) map.get("pageSize");
            String menuName = (String) map.get("menuName");

            LambdaQueryWrapper<Menu> wrapper = new LambdaQueryWrapper<>();
            wrapper.like(Menu::getMenuName, menuName);

            Page<Menu> page = new Page<>(current, pageSize);
            Page<Menu> menuPage = menuService.page(page, wrapper);
            JSONObject jsonObject = new JSONObject();
            jsonObject.putOnce("menuList", menuPage.getRecords());
            jsonObject.putOnce("total", menuPage.getTotal());
            return Result.success("菜单列表获取成功", jsonObject);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("菜单列表获取接口服务出错", null);
        }
    }

    @ApiOperation("菜单添加接口")
    @PostMapping("/addMenu")
    @Transactional(rollbackFor = Exception.class)
    public Result addMenu(@RequestBody Map<String, String> map, HttpServletResponse response) {
        try {
            String menuName = map.get("menuName");
            String menuPath = map.get("menuPath");
            if (!menuPath.startsWith("/")) {
                return Result.error("菜单添加失败，菜单路径应以/开头，且由英文字母示例：/test", null);
            } else {
                char[] chars = menuPath.substring(1).toCharArray();
                for (char ch : chars) {
                    if (!Character.isLetter(ch)) {
                        return Result.error("菜单添加失败，菜单路径应以/开头，且由英文字母示例：/test", null);
                    }
                }
            }

            LambdaQueryWrapper<Menu> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(Menu::getMenuName, menuName).or().eq(Menu::getMenuPath, menuPath);
            Menu menu = menuService.getOne(wrapper);
            if (menu != null) {
                return Result.error("菜单添加失败，该菜单名称、路径已存在，请重新添加", null);
            }

            for (Menu defaultMenu : DEFAULT_MENU_LIST) {
                if (defaultMenu.getMenuName().equals(menuName) || defaultMenu.getMenuPath().equals(menuPath)) {
                    return Result.error("菜单添加失败，该菜单名称、路径不可与默认菜单一致，请重新添加", null);
                }
            }

            File file = new File(VUE_PROJECT_PATH);
            if (!file.exists()) {
                Result.writeResult(response, "菜单添加失败，配置的vue项目路径不存在");
                throw new RuntimeException("菜单添加失败，配置的vue项目路径不存在");
            }
            String fileName = Character.toUpperCase(menuPath.charAt(1)) + menuPath.substring(2) + ".vue";
            File addVueFile = new File(VUE_PROJECT_PATH + fileName);
            try (FileOutputStream outputStream = new FileOutputStream(addVueFile)) {
                outputStream.write(DEFAULT_TEXT.getBytes(StandardCharsets.UTF_8));
            }

            menu = new Menu();
            menu.setMenuName(menuName);
            menu.setMenuPath(menuPath);
            menu.setFileName(fileName);
            menuService.save(menu);

            return Result.success("菜单添加成功", null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("菜单添加接口服务出错", null);
        }
    }

    @ApiOperation("菜单修改接口")
    @PostMapping("/editMenu")
    @Transactional(rollbackFor = Exception.class)
    public Result editMenu(@RequestBody Map<String, Object> map) {
        try {
            Integer menuId = (Integer) map.get("menuId");
            String menuName = (String) map.get("menuName");
            String menuPath = (String) map.get("menuPath");
            LambdaQueryWrapper<Menu> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(Menu::getMenuId, menuId);
            Menu menu = menuService.getOne(wrapper);
            if (menu == null) {
                return Result.error("该菜单不存在，修改失败", null);
            }

            LambdaQueryWrapper<Menu> notExistWrapper = new LambdaQueryWrapper<>();
            notExistWrapper.ne(Menu::getMenuId, menuId).and(menuWrapper -> menuWrapper.eq(Menu::getMenuName, menuName).or().eq(Menu::getMenuPath, menuPath));
            Menu notExist = menuService.getOne(notExistWrapper);
            if (notExist != null) {
                return Result.error("菜单修改失败，该菜单名称、路径已存在，请重新修改", null);
            }

            for (Menu defaultMenu : DEFAULT_MENU_LIST) {
                if (defaultMenu.getMenuName().equals(menuName) || defaultMenu.getMenuPath().equals(menuPath)) {
                    return Result.error("菜单添加失败，该菜单名称、路径不可与默认菜单一致，请重新添加", null);
                }
            }

            menu.setMenuName(menuName);
            menu.setMenuPath(menuPath);
            menuService.update(menu, wrapper);
            return Result.success("菜单修改成功", null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("菜单修改接口服务出错", null);
        }
    }

    @ApiOperation("菜单删除接口")
    @PostMapping("/deleteMenu")
    @Transactional(rollbackFor = Exception.class)
    public Result deleteMenu(@RequestBody Map<String, Object> map, HttpServletResponse response) {
        try {
            Integer menuId = (Integer) map.get("menuId");
            Menu menu = menuService.getMenuByMenuId(menuId);
            if (menu == null) {
                return Result.error("该菜单不存在，删除失败", null);
            }

            File file = new File(VUE_PROJECT_PATH);
            if (!file.exists()) {
                Result.writeResult(response, "菜单删除失败，配置的vue项目路径不存在");
                throw new RuntimeException("菜单删除失败，配置的vue项目路径不存在");
            }
            File deleteFile = new File(VUE_PROJECT_PATH + menu.getFileName());
            if (deleteFile.exists()) {
                boolean delete = deleteFile.delete();
                if (!delete) {
                    Result.writeResult(response, "菜单对应的vue文件删除失败，菜单删除失败");
                    throw new RuntimeException("菜单对应的vue文件删除失败，菜单删除失败");
                }
            }

            menuService.removeById(menu);
            return Result.success("菜单删除成功", null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("菜单删除接口服务出错", null);
        }
    }

    @ApiOperation("页面文件内容获取接口")
    @PostMapping("/showFileContent")
    public Result showFileContent(@RequestBody Map<String, Object> map, HttpServletResponse response) {
        try {
            Integer menuId = (Integer) map.get("menuId");
            Menu menu = menuService.getMenuByMenuId(menuId);
            if (menu == null) {
                return Result.error("该菜单不存在，删除失败", null);
            }
            File file = new File(VUE_PROJECT_PATH);
            if (!file.exists()) {
                Result.writeResult(response, "页面文件内容获取失败，配置的vue项目路径不存在");
                throw new RuntimeException("页面文件内容获取失败，配置的vue项目路径不存在");
            }
            File getFile = new File(VUE_PROJECT_PATH + menu.getFileName());
            if (!getFile.exists()) {
                Result.writeResult(response, "页面文件内容获取失败，页面文件不存在");
                throw new RuntimeException("页面文件内容获取失败，页面文件不存在:" + getFile.getAbsolutePath());
            }
            try (FileInputStream inputStream = new FileInputStream(getFile)) {
                byte[] buf = new byte[inputStream.available()];
                int read = inputStream.read(buf);
                return Result.success("页面文件内容获取成功", new String(buf, 0, read));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("页面文件内容获取接口服务出错", null);
        }
    }

    @ApiOperation("用户菜单页面源码下载接口")
    @GetMapping("/downloadUserPageCode")
    public void downloadUserPageCode(HttpServletResponse response) throws Exception {
        try {
            response.setContentType("application/zip");
            response.addHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("用户菜单页面源码.zip", "UTF-8"));
            try (ZipOutputStream outputStream = new ZipOutputStream(response.getOutputStream())) {
                downloadVueFile(outputStream);
                downloadImage(outputStream);
                downloadIco(outputStream);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response.reset();
            response.setContentType("application/octet-stream");
            response.addHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("用户菜单页面源码.txt", "UTF-8"));
            Result.writeResult(response, "用户菜单页面源码下载接口服务出错");
        }
    }

    private void downloadVueFile(ZipOutputStream outputStream) throws Exception {
        List<Menu> menuList = menuService.list();
        for (Menu menu : menuList) {
            File file = new File(VUE_PROJECT_PATH + menu.getFileName());
            if (!file.exists()) {
                throw new RuntimeException("页面文件不存在:" + file.getAbsolutePath() + "不存在，用户菜单页面源码下载失败");
            }
            outputStream.putNextEntry(new ZipEntry("src/views/" + file.getName()));
            try (FileInputStream inputStream = new FileInputStream(file)) {
                byte[] buf = new byte[inputStream.available()];
                int read = inputStream.read(buf);
                outputStream.write(buf, 0, read);
            }
            outputStream.closeEntry();
        }
    }

    private void downloadImage(ZipOutputStream outputStream) throws Exception {
        List<Image> imageList = imageService.list();
        for (Image image : imageList) {
            File file = new File(IMAGE_PATH + image.getImagePath());
            if (!file.exists()) {
                throw new RuntimeException("图片不存在:" + file.getAbsolutePath() + "不存在，用户菜单页面源码下载失败");
            }
            outputStream.putNextEntry(new ZipEntry("src/images/" + file.getName()));
            try (FileInputStream inputStream = new FileInputStream(file)) {
                byte[] buf = new byte[inputStream.available()];
                int read = inputStream.read(buf);
                outputStream.write(buf, 0, read);
            }
            outputStream.closeEntry();
        }
    }

    private void downloadIco(ZipOutputStream outputStream) throws Exception {
        List<Ico> icoList = icoService.list();
        for (Ico ico : icoList) {
            File file = new File(ICO_PATH + ico.getIcoPath());
            if (!file.exists()) {
                throw new RuntimeException("图标不存在:" + file.getAbsolutePath() + "不存在，用户菜单页面源码下载失败");
            }
            outputStream.putNextEntry(new ZipEntry("src/svgs/" + file.getName()));
            try (FileInputStream inputStream = new FileInputStream(file)) {
                byte[] buf = new byte[inputStream.available()];
                int read = inputStream.read(buf);
                outputStream.write(buf, 0, read);
            }
            outputStream.closeEntry();
        }
    }
}
