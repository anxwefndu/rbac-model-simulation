package com.boot.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.boot.entity.Result;
import com.boot.entity.User;
import com.boot.service.IUserService;
import com.boot.util.JwtUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.boot.config.WebConfig.DEFAULT_PASSWORD;
import static com.boot.config.WebConfig.DEFAULT_USERNAME;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author bbyh
 * @since 2023-07-11
 */
@RestController
@RequestMapping("/user")
@Api("用户接口")
@Slf4j
public class UserController {
    @Resource
    private IUserService userService;
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @ApiOperation("用户登录接口")
    @PostMapping("/login")
    public Result login(@RequestBody Map<String, String> map) {
        try {
            String username = map.get("username");
            String password = map.get("password");

            if (!DEFAULT_USERNAME.equals(username) || !DEFAULT_PASSWORD.equals(password)) {
                return Result.error("账号或密码错误", null);
            }
            String token = JwtUtil.getToken();
            stringRedisTemplate.opsForValue().set(username, token, 60 * 24, TimeUnit.MINUTES);
            return Result.success("登陆成功", token);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("用户登录接口服务出错", null);
        }
    }

    @ApiOperation("用户注册接口")
    @PostMapping("/register")
    @Transactional(rollbackFor = Exception.class)
    public Result register(@RequestBody Map<String, String> map) {
        try {
            String username = map.get("username");
            String password = map.get("password");

            LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(User::getUsername, username);
            User user = userService.getOne(wrapper);
            if (user != null) {
                return Result.error("该账号已存在", null);
            }
            user = new User();
            user.setUsername(username);
            user.setPassword(password);
            user.setRoleId(0);
            userService.save(user);

            return Result.success("注册成功", null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("用户注册接口服务出错", null);
        }
    }
}
