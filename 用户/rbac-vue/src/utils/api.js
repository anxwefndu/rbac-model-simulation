import axios from "axios";
import {ElMessage} from 'element-plus';

export const baseUrl = 'http://127.0.0.1:8081';

export function message(msg, type) {
    ElMessage({
        message: msg,
        showClose: true,
        type: type,
        center: true
    })
}

export const axiosRequest = async (method, path, params, data, success, error = (res) => {
    message(res.data.msg, "error");
}) => {
    const res = await axios({
        method: method,
        url: baseUrl + path,
        data: data,
        params: params,
        headers: {
            username: localStorage.getItem("username"),
            token: localStorage.getItem("token")
        }
    });
    if (res.data.code === 200) {
        success(res);
    } else if (res.data.code === 500) {
        error(res);
    }
}

export function getImage(imagePath) {
    return require("@/images/" + imagePath);
}

export function getIco(icoPath) {
    return require("@/svgs/" + icoPath);
}
