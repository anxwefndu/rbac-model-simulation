import {createRouter, createWebHashHistory} from 'vue-router'

import Home from '@/views/Home'
import Login from '@/views/Login'
import Register from '@/views/Register'
import Error from '@/views/Error'
import Index from '@/views/Index'
import {axiosRequest, message} from "@/utils/api";

const homeRoute = {
    path: '/home',
    name: "Home",
    component: Home,
    redirect: '/index',
    children: [
        {
            path: '/index',
            component: Index,
        },
    ]
};

export function initRoute(menuList) {
    homeRoute.children.splice(1, homeRoute.children.length - 1);
    for (let i = 0; i < menuList.length; i++) {
        if (menuList[i].menuId > 0) {
            homeRoute.children.push({
                path: menuList[i].menuPath,
                component: () => import("@/views/" + menuList[i].fileName)
            });
        }
    }
    router.removeRoute("Home");
    router.addRoute(homeRoute);
}

const routes = [
    {
        path: '/',
        redirect: '/login'
    },
    {
        path: '/login',
        component: Login,
    },
    {
        path: '/register',
        component: Register,
    },
    {
        path: '/:pathMatch(.*)',
        component: Error,
    },
]

const router = createRouter({
    history: createWebHashHistory(),
    routes
})

router.addRoute(homeRoute);

router.beforeEach(async (to, from, next) => {
    if (to.path === "/login" || to.path === "/register") {
        next();
    } else {
        await axiosRequest("get", "/user/verifyPath", {
            path: to.path
        }, null, () => {
            next();
        }, (res) => {
            message(res.data.msg, "error");
            next("/login");
        });
    }
});

export default router