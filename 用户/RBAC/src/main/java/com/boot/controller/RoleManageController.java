package com.boot.controller;

import com.boot.entity.Result;
import com.boot.service.IRoleMenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author bbyh
 * @since 2023-07-11
 */
@RestController
@RequestMapping("/roleManage")
@Api("角色接口")
@Slf4j
public class RoleManageController {
    @Resource
    private IRoleMenuService roleMenuService;

    @ApiOperation("用户菜单获取接口")
    @GetMapping("/getUserRoleMenu")
    public Result getUserRoleMenu(@RequestParam Integer roleId) {
        try {
            return Result.success("用户菜单获取成功", roleMenuService.getRoleMenu(roleId));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("用户菜单获取接口服务出错", null);
        }
    }
}
