package com.boot.controller;

import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.boot.entity.Result;
import com.boot.entity.User;
import com.boot.service.IUserService;
import com.boot.util.JwtUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.boot.config.WebConfig.ROLE_MENU_PREFIX;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author bbyh
 * @since 2023-07-11
 */
@RestController
@RequestMapping("/user")
@Api("用户接口")
@Slf4j
public class UserController {
    @Resource
    private IUserService userService;
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @ApiOperation("用户登录接口")
    @PostMapping("/login")
    public Result login(@RequestBody Map<String, String> map) {
        try {
            String username = map.get("username");
            String password = map.get("password");
            LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(User::getUsername, username).eq(User::getPassword, password);
            User user = userService.getOne(wrapper);
            if (user == null) {
                return Result.error("账号或密码错误", null);
            }
            String token = JwtUtil.getToken();
            stringRedisTemplate.opsForValue().set(username, token, 60 * 24, TimeUnit.MINUTES);
            userService.cacheMenuList(username, user.getRoleId());

            JSONObject jsonObject = new JSONObject();
            jsonObject.putOnce("token", token);
            jsonObject.putOnce("roleId", user.getRoleId());
            return Result.success("登陆成功", jsonObject);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("用户登录接口服务出错", null);
        }
    }

    @ApiOperation("用户注册接口")
    @PostMapping("/register")
    @Transactional(rollbackFor = Exception.class)
    public Result register(@RequestBody Map<String, String> map) {
        try {
            String username = map.get("username");
            String password = map.get("password");

            LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(User::getUsername, username);
            User user = userService.getOne(wrapper);
            if (user != null) {
                return Result.error("该账号已存在", null);
            }
            user = new User();
            user.setUsername(username);
            user.setPassword(password);
            user.setRoleId(0);
            userService.save(user);

            return Result.success("注册成功", null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("用户注册接口服务出错", null);
        }
    }

    @ApiOperation("访问路径校验接口")
    @GetMapping("/verifyPath")
    public Result verifyPath(@RequestParam String path, HttpServletRequest request) {
        try {
            String username = request.getHeader("username");
            String menuPathStr = stringRedisTemplate.opsForValue().get(ROLE_MENU_PREFIX + username);
            if (menuPathStr == null) {
                return Result.error("用户菜单不合法", null);
            }
            String[] menuPathArray = menuPathStr.split(",");
            for (String menuPath : menuPathArray) {
                if (menuPath.equals(path)) {
                    return Result.success("访问路径校验成功", null);
                }
            }
            return Result.error("该用户不具有该菜单的访问权限", null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("访问路径校验接口服务出错", null);
        }
    }
}
