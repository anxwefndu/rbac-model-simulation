package com.boot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.boot.entity.Image;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author bbyh
 * @since 2024-05-11
 */
public interface ImageMapper extends BaseMapper<Image> {

}
