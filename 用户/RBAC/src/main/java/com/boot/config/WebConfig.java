package com.boot.config;

import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.boot.interceptor.AuthorityInterceptor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author admin
 * @date 2023/3/31 0031 18:21
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {
    private static final List<String> AUTHORITY_EXCLUDE_PATH = new ArrayList<>();
    public static String ROLE_MENU_PREFIX;
    public static String DEFAULT_MENU_LIST;

    static {
        AUTHORITY_EXCLUDE_PATH.add("/user/login");
        AUTHORITY_EXCLUDE_PATH.add("/user/register");
        AUTHORITY_EXCLUDE_PATH.add("/user/verifyPath");
        AUTHORITY_EXCLUDE_PATH.add("/roleManage/getUserRoleMenu");
    }

    @Resource
    private AuthorityInterceptor authorityInterceptor;

    @Value("${default-menu-list}")
    public void setDefaultMenuList(String defaultMenuList) {
        DEFAULT_MENU_LIST = defaultMenuList;
    }

    @Value("${role-menu-prefix}")
    public void setRoleMenuPrefix(String roleMenuPrefix) {
        ROLE_MENU_PREFIX = roleMenuPrefix;
    }

    @Override
    public void addInterceptors(@NotNull InterceptorRegistry registry) {
        registry.addInterceptor(authorityInterceptor).addPathPatterns("/**").excludePathPatterns(AUTHORITY_EXCLUDE_PATH);
    }

    @Override
    public void addCorsMappings(@NotNull CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowedMethods("GET", "POST", "DELETE", "PUT")
                .maxAge(3600);
    }

    @Bean
    public MybatisPlusInterceptor paginationInterceptor() {
        MybatisPlusInterceptor mybatisPlusInterceptor = new MybatisPlusInterceptor();
        PaginationInnerInterceptor paginationInnerInterceptor = new PaginationInnerInterceptor();
        paginationInnerInterceptor.setOverflow(true);
        mybatisPlusInterceptor.addInnerInterceptor(paginationInnerInterceptor);
        return mybatisPlusInterceptor;
    }
}
