package com.boot.util;

import java.io.*;

/**
 * @author bbyh
 * @date 2024/5/11 14:43
 */
public class FileUtil {
    public static void upload(InputStream inputStream, String fileName, String path) {
        File destinationDir = new File(path);
        if (!destinationDir.exists()) {
            boolean mkdirs = destinationDir.mkdirs();
            if (!mkdirs) {
                throw new RuntimeException("目标文件夹创建失败:" + destinationDir);
            }
        }
        File file = new File(path + fileName);
        try (FileOutputStream outputStream = new FileOutputStream(file)) {
            int available = inputStream.available();
            byte[] buf = new byte[available];
            int read = inputStream.read(buf);
            outputStream.write(buf, 0, read);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void download(OutputStream outputStream, String fileName, String path) {
        File file = new File(path + fileName);
        if (file.exists()) {
            try (FileInputStream inputStream = new FileInputStream(file)) {
                byte[] buf = new byte[inputStream.available()];
                int read = inputStream.read(buf);
                outputStream.write(buf, 0, read);
            } catch (Exception e) {
                throw new RuntimeException("文件下载失败：" + file.getAbsolutePath());
            }
        } else {
            throw new RuntimeException("文件不存在：" + file.getAbsolutePath());
        }
    }

    public static void delete(String fileName, String path) {
        File file = new File(path + fileName);
        if (file.exists()) {
            boolean delete = file.delete();
            if (!delete) {
                throw new RuntimeException("文件删除失败：" + file.getAbsolutePath());
            }
        } else {
            throw new RuntimeException("文件不存在：" + file.getAbsolutePath());
        }
    }
}
