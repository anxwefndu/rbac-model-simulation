package com.boot.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;

import java.util.Calendar;

/**
 * @author bbyh
 * @since 2024-04-27
 */
public class JwtUtil {
    private static final String SECRET = "123ABC!@#*()&^";

    public static String getToken() {
        Calendar instance = Calendar.getInstance();
        instance.add(Calendar.MINUTE, 60 * 24);
        JWTCreator.Builder builder = JWT.create();
        return builder.withExpiresAt(instance.getTime()).sign(Algorithm.HMAC256(SECRET));
    }

    public static void verify(String token) {
        JWT.require(Algorithm.HMAC256(SECRET)).build().verify(token);
    }
}
