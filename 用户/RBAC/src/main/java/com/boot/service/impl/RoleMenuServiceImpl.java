package com.boot.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.boot.entity.Menu;
import com.boot.entity.RoleMenu;
import com.boot.mapper.RoleMenuMapper;
import com.boot.service.IMenuService;
import com.boot.service.IRoleMenuService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author bbyh
 * @since 2023-07-11
 */
@Service
public class RoleMenuServiceImpl extends ServiceImpl<RoleMenuMapper, RoleMenu> implements IRoleMenuService {
    @Resource
    private IMenuService menuService;

    @Override
    public List<Menu> getRoleMenu(Integer roleId) {
        LambdaQueryWrapper<RoleMenu> roleMenuQueryWrapper = new LambdaQueryWrapper<>();
        roleMenuQueryWrapper.eq(RoleMenu::getRoleId, roleId);
        List<RoleMenu> roleMenuList = list(roleMenuQueryWrapper);
        List<Integer> menuIds = new ArrayList<>();
        for (RoleMenu roleMenu : roleMenuList) {
            menuIds.add(roleMenu.getMenuId());
        }

        List<Menu> menuList = new ArrayList<>();
        if (menuIds.size() > 0) {
            LambdaQueryWrapper<Menu> menuQueryWrapper = new LambdaQueryWrapper<>();
            menuQueryWrapper.in(Menu::getMenuId, menuIds);
            menuList = menuService.list(menuQueryWrapper);
        }
        return menuList;
    }
}
