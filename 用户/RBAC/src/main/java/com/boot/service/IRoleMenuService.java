package com.boot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.boot.entity.Menu;
import com.boot.entity.RoleMenu;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author bbyh
 * @since 2023-07-11
 */
public interface IRoleMenuService extends IService<RoleMenu> {
    List<Menu> getRoleMenu(Integer roleId);
}
