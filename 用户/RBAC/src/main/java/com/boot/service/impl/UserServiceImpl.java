package com.boot.service.impl;

import cn.hutool.core.text.StrBuilder;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.boot.entity.Menu;
import com.boot.entity.User;
import com.boot.mapper.UserMapper;
import com.boot.service.IRoleMenuService;
import com.boot.service.IUserService;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.boot.config.WebConfig.DEFAULT_MENU_LIST;
import static com.boot.config.WebConfig.ROLE_MENU_PREFIX;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author bbyh
 * @since 2023-07-11
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {
    @Resource
    private IRoleMenuService roleMenuService;
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public void cacheMenuList(String username, Integer roleId) {
        List<Menu> menuList = roleMenuService.getRoleMenu(roleId);
        StrBuilder builder = new StrBuilder();
        for (Menu menu : menuList) {
            builder.append(menu.getMenuPath()).append(",");
        }
        builder.append(DEFAULT_MENU_LIST);
        stringRedisTemplate.opsForValue().set(ROLE_MENU_PREFIX + username, builder.toString(), 60 * 24, TimeUnit.MINUTES);
    }
}
