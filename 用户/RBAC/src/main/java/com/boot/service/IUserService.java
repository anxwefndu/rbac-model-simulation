package com.boot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.boot.entity.User;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author bbyh
 * @since 2023-07-11
 */
public interface IUserService extends IService<User> {
    void cacheMenuList(String username, Integer roleId);
}
