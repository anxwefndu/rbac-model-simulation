package com.boot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.boot.entity.Menu;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author bbyh
 * @since 2023-07-11
 */
public interface IMenuService extends IService<Menu> {

}
