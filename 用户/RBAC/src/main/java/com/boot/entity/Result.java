package com.boot.entity;

import cn.hutool.json.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author bbyh
 * @date 2023/6/19 18:22
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
public class Result {
    private static final Integer SUCCESS = 200;
    private static final Integer ERROR = 500;

    public int code;
    public String msg;
    public Object data;

    public static Result success(String msg, Object data) {
        return new Result(SUCCESS, msg, data);
    }

    public static Result error(String msg, Object data) {
        return new Result(ERROR, msg, data);
    }

    public static void writeResult(HttpServletResponse response, String msg) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.set("code", 500);
            jsonObject.set("msg", msg);
            response.setContentType("application/json,charset=utf-8");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().print(jsonObject);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }
}
