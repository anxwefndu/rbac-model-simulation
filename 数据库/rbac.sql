DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `menu_id` int NOT NULL AUTO_INCREMENT COMMENT '菜单编号',
  `menu_name` varchar(20) NOT NULL COMMENT '菜单名字',
  `menu_path` varchar(20) NOT NULL COMMENT '菜单路径',
  `file_name` varchar(20) NOT NULL COMMENT '菜单名字',
  PRIMARY KEY (`menu_id`)
) ;


DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `role_id` int NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(20) NOT NULL COMMENT '权限名称',
  PRIMARY KEY (`role_id`)
);


DROP TABLE IF EXISTS `role_menu`;
CREATE TABLE `role_menu`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `role_id` int NOT NULL COMMENT '角色ID',
  `menu_id` int NOT NULL COMMENT '一级菜单编号',
  PRIMARY KEY (`id`)
);


DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `username` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(20) NOT NULL COMMENT '密码',
  `role_id` int NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`)
);


DROP TABLE IF EXISTS `image`;
CREATE TABLE `image`  (
  `image_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '图片ID',
  `image_name` varchar(50) NOT NULL COMMENT '图片名称',
  `image_path` varchar(50) NOT NULL COMMENT '图片保存路径',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`image_id`)
);


DROP TABLE IF EXISTS `ico`;
CREATE TABLE `ico`  (
  `ico_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '图标ID',
  `ico_name` varchar(50) NOT NULL COMMENT '图标名称',
  `ico_path` varchar(50) NOT NULL COMMENT '图标保存路径',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`ico_id`)
);
